package br.com.hiperxp.iptv.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import br.com.hiperxp.iptv.IptvDatabase;
import br.com.hiperxp.iptv.dao.HomeBannerDao;
import br.com.hiperxp.iptv.model.HomeBanner;

public class HomeBannerRepository {
    private HomeBannerDao homeBannerDao;
    private LiveData<List<HomeBanner>> banners;

    public HomeBannerRepository(Application application) {
        IptvDatabase database = IptvDatabase.getInstance(application);
        homeBannerDao = database.homeBannerDao();
        banners = homeBannerDao.getAllHomeBanner();
    }

    public void insert(HomeBanner homeBanner) {
        new InsertHomeBannerAsyncTask(homeBannerDao).execute(homeBanner);
    }

    public void update(HomeBanner homeBanner) {
        new UpdateHomeBannerAsyncTask(homeBannerDao).execute(homeBanner);
    }

    public void delete(HomeBanner homeBanner) {
        new DeleteHomeBannerAsyncTask(homeBannerDao).execute(homeBanner);
    }

    public void deleteAll(HomeBanner homeBanner) {
        new DeleteAllHomeBannerAsyncTask(homeBannerDao).execute();
    }

    public LiveData<List<HomeBanner>> getAllhomeBanner() {
        return banners;
    }

    private static class InsertHomeBannerAsyncTask extends AsyncTask<HomeBanner, Void, Void> {
        private HomeBannerDao homeBannerDao;
        private InsertHomeBannerAsyncTask(HomeBannerDao homeBannerDao){
            this.homeBannerDao = homeBannerDao;
        }
        @Override
        protected Void doInBackground(HomeBanner... banners) {
            homeBannerDao.insert(banners[0]);
            return null;
        }
    }

    private static class UpdateHomeBannerAsyncTask extends AsyncTask<HomeBanner, Void, Void> {
        private HomeBannerDao homeBannerDao;
        private UpdateHomeBannerAsyncTask(HomeBannerDao homeBannerDao){
            this.homeBannerDao = homeBannerDao;
        }
        @Override
        protected Void doInBackground(HomeBanner... homeBanners) {
            homeBannerDao.update(homeBanners[0]);
            return null;
        }
    }

    private static class DeleteHomeBannerAsyncTask extends AsyncTask<HomeBanner, Void, Void> {
        private HomeBannerDao homeBannerDao;
        private DeleteHomeBannerAsyncTask(HomeBannerDao homeBannerDao){
            this.homeBannerDao = homeBannerDao;
        }
        @Override
        protected Void doInBackground(HomeBanner... homeBanners) {
            homeBannerDao.insert(homeBanners[0]);
            return null;
        }
    }

    private static class DeleteAllHomeBannerAsyncTask extends AsyncTask<Void, Void, Void> {
        private HomeBannerDao homeBannerDao;
        private DeleteAllHomeBannerAsyncTask(HomeBannerDao homeBannerDao){
            this.homeBannerDao = homeBannerDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            homeBannerDao.deleteAllHomeBanner();
            return null;
        }
    }
}
