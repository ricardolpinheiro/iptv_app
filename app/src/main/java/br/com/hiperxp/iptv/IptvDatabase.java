package br.com.hiperxp.iptv;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import br.com.hiperxp.iptv.dao.HomeBannerDao;
import br.com.hiperxp.iptv.dao.UserDao;
import br.com.hiperxp.iptv.model.HomeBanner;
import br.com.hiperxp.iptv.model.User;

@Database(entities = {User.class, HomeBanner.class}, version = 1)
public abstract class IptvDatabase extends RoomDatabase {

    private static IptvDatabase instance;

    public abstract UserDao userDao();

    public abstract HomeBannerDao homeBannerDao();

    public static synchronized IptvDatabase getInstance(Context context){
        if(instance == null) {
            instance = Room.databaseBuilder(context,
                    IptvDatabase.class, "iptv_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }

        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private UserDao userDao;

        private PopulateDbAsyncTask(IptvDatabase db) {
            userDao = db.userDao();
        }


        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }
    }
}
