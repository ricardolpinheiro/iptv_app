package br.com.hiperxp.iptv.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import br.com.hiperxp.iptv.model.HomeBanner;

@Dao
public interface HomeBannerDao {

    @Insert
    void insert(HomeBanner user);

    @Update
    void update(HomeBanner user);

    @Delete
    void delete(HomeBanner user);

    @Query("DELETE FROM home_banner_table")
    void deleteAllHomeBanner();

    @Query("SELECT * FROM home_banner_table")
    LiveData<List<HomeBanner>> getAllHomeBanner();

}
