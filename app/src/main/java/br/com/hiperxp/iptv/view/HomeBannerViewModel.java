package br.com.hiperxp.iptv.view;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import br.com.hiperxp.iptv.model.HomeBanner;
import br.com.hiperxp.iptv.model.User;
import br.com.hiperxp.iptv.repository.HomeBannerRepository;
import br.com.hiperxp.iptv.repository.UserRepository;

public class HomeBannerViewModel extends AndroidViewModel {
    private HomeBannerRepository homeBannerRepository;
    private LiveData<List<HomeBanner>> banners;

    public HomeBannerViewModel(@NonNull Application application) {
        super(application);
        homeBannerRepository = new HomeBannerRepository(application);
        banners = homeBannerRepository.getAllhomeBanner();
    }

    public void insert(HomeBanner homeBanner) {
        homeBannerRepository.insert(homeBanner);
    }

    public void update(HomeBanner homeBanner) {
        homeBannerRepository.update(homeBanner);
    }

    public void delete(HomeBanner homeBanner) {
        homeBannerRepository.delete(homeBanner);
    }

    public void deleteAll(HomeBanner homeBanner) {
        homeBannerRepository.deleteAll(homeBanner);
    }

    public LiveData<List<HomeBanner>> getAllHomeBanner() {
        return banners;
    }
}
