package br.com.hiperxp.iptv.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "home_banner_table")
public class HomeBanner {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String url;

    private int version;

    @Ignore
    private List<String> list_url;

    public HomeBanner(String url, int version) {
        this.url = url;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<String> getList_url() {
        return list_url;
    }

    public void setList_url(List<String> list_url) {
        this.list_url = list_url;
    }
}
